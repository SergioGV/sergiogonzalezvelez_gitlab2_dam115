package sergiogonzálezvélez_gitlab2_dam115;

import java.util.Scanner;

public class SergioGonzálezVélez_GitLab2_DAM115 {

    public static void main(String[] args) {
        double km, m;
        int h, s;
        int opt;
        Scanner sc = new Scanner(System.in);
        do {
            System.out.println("Introduce las horas:");
            h = sc.nextInt();
            if (h <= 0) {
                System.out.println("Cantidad inválida.");
            }
        } while (h <= 0);
        do {
            System.out.println("Introduce los kilometros:");
            km = sc.nextDouble();
            if (km <= 0) {
                System.out.println("Cantidad inválida.");
            }
        } while (km <= 0);
        double v = km / h;
        System.out.println("La velocidad es "+v+"km/h.");
        do {
            System.out.println("Elige la opción que deseas realizar:");
            System.out.println("1. Horas --- Segundos");
            System.out.println("2. Kilometros --- Metros");
            System.out.println("3. Kilometros/Hora --- Metros/Segundo");
            System.out.println("4. Salir");
            opt = sc.nextInt();
            if (opt < 1 || opt > 4) {
                System.out.println("Opción incorrecta.");
            }
        } while (opt != 1 && opt != 2 && opt != 3 && opt!=4);
        switch (opt) {
            case 1:
                s = h * 3600;
                System.out.println(h + "h equivalen a " + s + "s.");
                break;
            case 2:
                m = km * 1000;
                System.out.println(km + "km equivalen a " + m + "m.");
                break;
            case 4:
                System.out.println("PROGRAMA FINALIZADO");
                break;
            case 3:
                v=v/3.6;
                System.out.println(km+"/"+h+"km/h equivale a "+v+"m/s.");
        }
    }

}
